package com.fira_apps.maksi.Model;

public class Makanan {
    private int gambar;
    private String nama_makanan;
    private String asal_daerah;
    private String deskripsi;

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getAsal_daerah() {
        return asal_daerah;
    }

    public void setAsal_daerah(String asal_daerah) {
        this.asal_daerah = asal_daerah;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getGambar() {
        return gambar;
    }

    public void setGambar(int gambar) {
        this.gambar = gambar;
    }
}
