package com.fira_apps.maksi.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fira_apps.maksi.Model.Makanan;
import com.fira_apps.maksi.R;

import java.util.ArrayList;

public class ListMakananAdapter extends RecyclerView.Adapter<ListMakananAdapter.ListViewHolder> {
    private ArrayList<Makanan> listMakanan;
    private OnItemClickCallback onItemClickCallback;

    public ListMakananAdapter(ArrayList<Makanan> list) {
        this.listMakanan = list;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_makanan, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        Makanan makanan = listMakanan.get(position);
        Glide.with(holder.itemView.getContext())
                .load(makanan.getGambar())
                .apply(new RequestOptions().override(80,80))
                .into(holder.iv_makanan);
        holder.tv_nama_makanan.setText(makanan.getNama_makanan());
        holder.tv_asal_daerah.setText(makanan.getAsal_daerah());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClicked(listMakanan.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMakanan.size();
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_makanan;
        TextView tv_nama_makanan, tv_asal_daerah;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_makanan = itemView.findViewById(R.id.iv_makanan);
            tv_nama_makanan = itemView.findViewById(R.id.tv_nama_makanan);
            tv_asal_daerah = itemView.findViewById(R.id.tv_asal_daerah);
        }
    }

    public interface OnItemClickCallback{
        void onItemClicked(Makanan data);
    }
}
