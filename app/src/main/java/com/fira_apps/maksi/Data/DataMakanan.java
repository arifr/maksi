package com.fira_apps.maksi.Data;

import com.fira_apps.maksi.Model.Makanan;
import com.fira_apps.maksi.R;

import java.util.ArrayList;

public class DataMakanan {
    private static int[] arrGambar = {
            R.drawable.img_mie_aceh,
            R.drawable.img_bika_ambon,
            R.drawable.img_rendang,
            R.drawable.img_otak_otak,
            R.drawable.img_pempek,
            R.drawable.img_sate_bandeng,
            R.drawable.img_kerak_telor,
            R.drawable.img_peuyeum,
            R.drawable.img_lumpia,
            R.drawable.img_gudeg
    };

    private static String[] arrNamaMakanan = {
            "Mie Aceh",
            "Bika Ambon",
            "Rendang",
            "Otak-otak",
            "Pempek",
            "Sate Bandeng",
            "Kerak Telor",
            "Peuyeum",
            "Lumpia",
            "Gudeg"
    };

    private static String[] arrAsalDaerah = {
            "Aceh",
            "Sumatera Utara",
            "Sumatera Barat",
            "Kepulauan Riau",
            "Sumatera Selatan",
            "Banten",
            "Jakarta",
            "Bandung",
            "Semarang",
            "Yogyakarta"
    };

    private static String[] arrDeskripsi = {
            "Makanan ini memang sangat terkenal bukan hanya di tengah masyarakat aceh saja melainkan juga dikenal oleh masyarakat luar aceh karena citarasanya sangat istimewa. Rasa makanan ini memang sangat kaya rempah sehingga terasa begitu nikmat.",
            "Makanan ini banyak dikira sebagai makanan khas Ambon padahal berasal dari Sumatera. Bika ambon merupakan kue yang banyak di jual di berbagai wilayah di indonesia karena memang banyak digemari masyarakat Indonesia. Rasa bika ambon biasanya rasa durian, keju, dan yang lainnya.",
            "Rendang terbuat dari daging sapi yang dimasak dengan bumbu rempah yang banyak sehingga rasanya sangat lezat dan kuat. Anda bisa menemukan makanan ini di rumah makan khas padang atau di tempat lainnya yang memang menyediakan menu khas padang.",
            "Makanan ini memang sangat khas karena terbuat dari bahan ikan dan cumi yang diberi bumbu pedas. Aroma dari salah satu makanan khas 34 provinsi dan juga gambarnya sangat lezat karena selain terbuat dari ikan makanan ini juga dimasak dengan dibungkus daun pisang dan kemudian akan dibakar.",
            "Makanan ini memang sangat lezat karena terbuat dari bahan sagu dan ikan yang kemudian akan di sajikan dengan kuah dari air, gula merah, cabe, bawang, garam, udang ebi dan sedikit cuka. Pempek sangat nikmat dan gurih karena terasa kenyal dan juga begitu terasa sekali aroma ikannya.",
            "Jika biasanya sate terbuat dari daging ayam, kambing atau kelinci, makanan ini terbuat dari bandeng yang empuk dan tidak bertulang. Bahan yang akan melengkapi sate bandeng adalah kuah santan dan gula coklat yang lezat.",
            "Kerak telur adalah makanan asli daerah Jakarta, dengan bahan-bahan beras ketan putih, telur ayam, ebi yang disangrai kering ditambah bawang merah goreng, lalu diberi bumbu yang dihaluskan berupa kelapa sangrai, cabai merah, kencur, jahe, merica butiran, garam dan gula pasir.",
            "Peyeum merupakan makanan Khas Bandung yang terbuat dari singkong.Konon, zaman dahulu para warga Bandung memanfaatkan tanaman singkong yang melimpah di tanah Sunda ini sebagai makanan untuk menghangatkan.",
            "Lumpia semarang adalah makanan semacam rollade yang berisi rebung, telur, dan daging ayam atau udang. Cita rasa lumpia semarang adalah perpaduan rasa antara Tionghoa dan Indonesia karena pertama kali dibuat oleh seorang keturunan Tionghoa yang menikah dengan orang Indonesia dan menetap di Semarang, Jawa Tengah.",
            "Gudeg adalah makanan khas Yogyakarta dan Jawa Tengah yang terbuat dari nangka muda yang dimasak dengan santan. Perlu waktu berjam-jam untuk membuat masakan ini. Warna coklat biasanya dihasilkan oleh daun jati yang dimasak bersamaan."
    };

    public static ArrayList<Makanan> getListData() {
        ArrayList<Makanan> list = new ArrayList<>();
        for (int position = 0; position < arrNamaMakanan.length; position++) {
            Makanan makanan = new Makanan();
            makanan.setGambar(arrGambar[position]);
            makanan.setNama_makanan(arrNamaMakanan[position]);
            makanan.setAsal_daerah(arrAsalDaerah[position]);
            makanan.setDeskripsi(arrDeskripsi[position]);
            list.add(makanan);
        }
        return list;
    }
}
